# article-management-mean-mongodb

My cool TypeScript app

## Quick Start

Get started developing...

```shell
# install deps
npm install

# run in development mode
npm run dev

# run tests
npm run test
```

---

## How do I modify the example API and make it my own?

There are two key files:
1. `server/routes.ts` - This references the implementation of all of your routes. Add as many routes as you like and point each route your express handler functions.
2. `server/common/api.yaml` - This file contains your [OpenAPI spec](https://swagger.io/specification/). Describe your API here. It's recommended that you to declare any and all validation logic in this YAML. `express-no-stress-typescript`  uses [express-openapi-validator](https://github.com/cdimascio/express-openapi-validator) to automatically handle all API validation based on what you've defined in the spec.

## Install Dependencies

Install all package dependencies (one time operation)

```shell
npm install
```

## Run It
#### Run in *development* mode:
Runs the application is development mode. Should not be used in production

```shell
npm run dev
```

or debug it

```shell
npm run dev:debug
```

#### Run in *production* mode:

Compiles the application and starts it in production production mode.

```shell
npm run compile
npm start
```

## Test It

Run the Mocha unit tests

```shell
npm test
```

or debug them

```shell
npm run test:debug
```

## Try It
* Open your browser to [http://localhost:3000](http://localhost:3000)
* Invoke the `/examples` endpoint 
  ```shell
  curl http://localhost:3000/api/v1/examples
  ```


## Debug It

#### Debug the server:

```
npm run dev:debug
```

#### Debug Tests

```
npm run test:debug
```


#### Environment Variables

Add this to the .env file for local access to the docker-compose db instance.

@todo Move the running of the site in to a container managed by compose so we don't need to define the .env file

```text
## Environment ##
APP_ID=article-management-mean-mysql
LOG_LEVEL=debug
REQUEST_LIMIT=100kb
SESSION_SECRET=mySecret
OPENAPI_SPEC=/api/v1/spec

## Environment ##
NODE_ENV=dev

## Server ##
NODE_PORT=3000

# MONGOOSE
DB_ADAPTER=mongodb (mongodb+srv)
DB_HOST=127.0.0.1
DB_USER=
DB_PASSWORD=
DB_DATABASE=article
DB_PORT=27017
```

