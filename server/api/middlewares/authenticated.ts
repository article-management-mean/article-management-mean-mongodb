import { IJwtExtended } from '../../common/types';
import { check as checkAuthenticated, getJwt } from '../../common/jwt';
import { NextFunction, Request, Response } from 'express';
// import responseHandler from '../../common/response.handler';
import requestHandler from '../../common/request.handler';

export const check = (jwt: string): IJwtExtended => {
  // get the jwt token
  const payload = checkAuthenticated(jwt);
  return payload;
};

export default (req: Request, _res: Response, next: NextFunction) => {
  try {
    // check the token
    const jwt = getJwt(req);
    const checkJwt = check(jwt);
    // req.body.jwt = checkJwt;
    // all good so continue
    if (checkJwt) {
      next();
    }
  } catch (e) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    requestHandler(req);
  }
};
