import { check } from 'express-validator';
import { Document, Schema } from 'mongoose';
import { hash } from '../../common/password';
import Model from '../config/model';
import create from '../middlewares/validate';
import isUniqueEmail from '../validators/isUniqueEmail';
import passwordValidator from '../validators/password';

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmation: string;
}

export const UsersSchema = new Schema({
  id: Number,
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  confirmation: String,
  created: Date,
  updated: Date,
});

export interface ILoginRequest {
  email: string;
  password: string;
}

export const messageUnauthorized =
  'Email and password combination are incorrect';

export const validateRegister = create([
  check('email')
    .not()
    .isEmpty()
    .withMessage('Email is required')
    .isEmail()
    .withMessage('Email is invalid')
    .custom(isUniqueEmail),
  check('password')
    .not()
    .isEmpty()
    .withMessage('Password is required')
    .custom(passwordValidator),

  check('firstName').not().isEmpty().withMessage('First name is required'),

  check('lastName').not().isEmpty().withMessage('Last name is required'),
]);

export const validateLogin = create([
  check('email')
    .not()
    .isEmpty()
    .withMessage('Email is required')
    .isEmail()
    .withMessage('Email is invalid'),

  check('password').not().isEmpty().withMessage('Password is required'),
]);

export default class Users extends Model<IUser> {
  public getModelName = (): string => {
    return 'User';
  };

  public getModelSchema = (): Schema => {
    return UsersSchema;
  };

  /**
   * Get the User model by the email address
   *
   * @param email
   */
  public getByEmail = async (email: string) => this.getBy({ email });

  /**
   * Called before the model is saved
   *
   * @param model
   */
  protected beforeSet = async (model: Partial<IUser>) => {
    const values = { ...model };

    // hash the password if one was provided
    if (typeof model.password === 'string') {
      values.password = await this.beforeSetPassword(model.password);
    }

    return values;
  };

  /**
   * Hash the password if it's currently a plain string
   *
   * @param password
   */
  private beforeSetPassword = async (password: IUser['password']) =>
    await hash(password);
}
