import { check } from 'express-validator';
import { Schema } from 'mongoose';
import Model from '../config/model';
import create from '../middlewares/validate';

export const ArticleSchema = new Schema({
  id: Number,
  title: { type: String, required: true },
  image: String,
  description: { type: String, required: true },
  publishDate: Date,
  created: Date,
  updated: Date,
});
export interface IArticle {
  id: number;
  title: string;
  image: string;
  description: string;
  publishDate?: string;
}

type ArticleDocument = IArticle & Document;

export const validate = create([
  check('title').not().isEmpty().withMessage('Article title is required'),
  check('description')
    .not()
    .isEmpty()
    .withMessage('Article description is required'),
]);

export default class Articles extends Model<ArticleDocument> {
  public getModelName = (): string => {
    return 'Articles';
  };

  public getModelSchema = (): Schema => {
    return ArticleSchema;
  };
}
