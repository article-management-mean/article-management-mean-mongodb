import mongoose, { Document } from 'mongoose';
import { DBModel } from './db';

/**
 * Model with dates interface
 */
export interface IModelWithDates {
  DateCreated: Date;
  DateDeleted?: Date;
  DateModified: Date;
}

/**
 * Base model class for interacting with the data store
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default abstract class Model<T = any, _K = number> {
  /**
   * Get the model name which is also used as the table name
   */
  public abstract getModelName(): string;

  /**
   * Get the model schema which is also used as the table structure
   */
  public abstract getModelSchema(): mongoose.Schema;

  /**
   * Save the model to the store
   *
   * @param model
   */
  public create = async (model: Partial<T>) => {
    const Model = this.query();
    return new Model(
      await this.beforeSet({
        created: new Date(),
        updated: new Date(),
        ...model,
      })
    ).save();
  };

  /**
   * Perform a simple fetch on the model and return the results
   */
  public fetch = async (search: Partial<any>) =>
    this.query().find(search).exec();

  /**
   * Fetch all of the rows from the table
   */
  public fetchAll = async () => this.query().find().exec();

  /**
   * Perform a primary key lookup on the model and return a single result
   *
   * @param id
   */
  public get = async (id: number) => this.getBy({ id });

  /**
   * Get a model by the where
   *
   * @param where
   */
  public getBy = async (where: any) => this.query().findOne(where).exec();

  /**
   * Perform a query on the dataset
   */
  public query = () => {
    return DBModel<T & Document<any>>(
      this.getModelName(),
      this.getModelSchema()
    );
  };

  /**
   * Update the model
   *
   * @param where
   * @param model
   */
  public update = async (where: Partial<any>, model: Partial<any>) =>
    this.query()
      .findOne(where)
      .update(
        await this.beforeSet({
          updated: new Date(),
          ...model,
        })
      );

  /**
   * Delete the model
   *
   * @param where
   */
  public delete = async (where: Partial<any>) =>
    this.query().findOneAndDelete(where).exec();

  /**
   * Override to allow fields to be changed before updating a model
   * and saving to the database
   *
   * @param model
   */
  // eslint-disable-next-line @typescript-eslint/require-await
  protected beforeSet = async (model: Partial<any>) => model;
}
