/**
 * Get the value from the environment
 *
 * @param name
 * @param defaultValue
 */
export default (name: string, defaultValue = ''): string =>
  typeof process.env[name] === 'string'
    ? (process.env[name] as string)
    : defaultValue;
