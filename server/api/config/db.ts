import env from './env';
import mongoose from 'mongoose';
import { IMongoDBConfig } from '../../common/types';

const config: IMongoDBConfig = {
  adapter: env('DB_ADAPTER'),
  database: env('DB_DATABASE'),
  host: env('DB_HOST'),
  password: env('DB_PASSWORD'),
  port: Number(env('DB_PORT')),
  user: env('DB_USER'),
};

export const Schema = mongoose.Schema;
export const DBModel = mongoose.model;

let mongoDB = '';
if (env('DB_PORT')) {
  // eslint-disable-next-line max-len
  mongoDB = `${config.adapter}://${config.user}:${config.password}@${config.host}:${config.port}/${config.database}`;
} else {
  // eslint-disable-next-line max-len
  mongoDB = `${config.adapter}://${config.user}:${config.password}@${config.host}/${config.database}`;
}

console.log('mongoDB: ', mongoDB);
export default mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
