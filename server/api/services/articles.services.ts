import Articles, { IArticle } from '../entities/articles';

class ArticlesService {
  /**
   * @param id
   */
  public async getOne(id: number): Promise<IArticle | null> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await new Articles().getBy({
      id,
    });
  }

  /**
   *
   */
  public async getAll(): Promise<IArticle[]> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await new Articles().fetchAll();
  }

  /**
   *
   * @param data
   */
  public async add(data: IArticle): Promise<IArticle> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await new Articles().create(data);
  }

  /**
   *
   * @param id
   * @param data
   */

  public async update(id: number, data: any): Promise<IArticle | null> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await new Articles().update(
      {
        id,
      },
      data
    );
  }

  /**
   *
   * @param id
   */
  public async delete(id: number): Promise<IArticle | null> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await new Articles().delete({
      id,
    });
  }
}

export default new ArticlesService();
