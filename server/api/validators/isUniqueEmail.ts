import Register from '../entities/users';

export const messageInUse = 'Email is already in use';

export default async (email: string): Promise<boolean> => {
  if (!email) {
    return false;
  }

  // does this email already exist in the user table?
  const result = await new Register().getByEmail(email);
  if (result !== undefined && result !== null) {
    // email address is already in use
    throw new Error(messageInUse);
  }

  return true;
};
