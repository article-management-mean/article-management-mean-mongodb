import badRequest from './badRequest';
import notFound from './notFound';
import request from './request';
import unauthorized from './unauthorized';

export const BadRequestError = badRequest;
export const NotFoundError = notFound;
export const RequestError = request;
export const UnauthorizedError = unauthorized;
