import Request from './request';

export default class BadRequest extends Request {
  constructor(message: string, statusCode: number = 400) {
    super(message, statusCode);
  }
}
