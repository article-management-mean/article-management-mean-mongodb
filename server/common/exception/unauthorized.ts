import Request from './request';

export default class Unauthorized extends Request {
  constructor(message: string, statusCode: number = 401) {
    super(message, statusCode);
  }
}
