export interface IResponseError {
  errors: IResponseErrorMessage[];
}

export interface IResponseErrorMessage {
  msg: string;
  param: string;
  value?: any;
}

export interface IJwt {
  data: {
    userId: number;
    email: string;
  };
  iat: number;
  exp: number;
}

export interface IJwtExtended extends IJwt {
  token: string;
  expiresIn: number;
}

export interface IMongoDBConfig {
  adapter: string;
  database: string;
  host: string;
  password: string;
  port: number;
  user: string;
}
