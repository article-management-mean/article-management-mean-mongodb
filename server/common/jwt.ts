/* eslint-disable @typescript-eslint/no-unsafe-call */
import addSeconds from 'date-fns/addSeconds';
import getUnixTime from 'date-fns/getUnixTime';
import { Request } from 'express';
import jwt from 'jsonwebtoken';
import env from '../api/config/env';

import { jwtLife } from './constants';
import { BadRequestError, UnauthorizedError } from './exception';
import { IJwt, IJwtExtended } from './types';

export const messageInvalidJwt = 'Invalid JWT provided';
export const messageJwtDeleted = 'JWT has been deleted';
export const messageJwtExpired = 'JWT has expired';
export const messageNoJwt = 'No JWT provided';

export const getJwt = (req: Request) => {
  const { body, headers, query } = req;
  let token = '';

  token = headers.authorization || '';
  if (token.startsWith('Bearer ')) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }

  if (!token) {
    token = body?.jwt || body?.token || query?.jwt || query?.token;
  }

  return token;
};

export const getTokenExpiry = (): number =>
  getUnixTime(addSeconds(new Date(), jwtLife));

export const generateJWT = (
  userId: number,
  email: string,
  extra: any = {}
): string => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  const data = { userId, email, ...extra };
  const expiresIn = getTokenExpiry();
  const sign = jwt.sign(
    {
      data,
    },
    env('SESSION_SECRET', 'mySecret'),
    {
      expiresIn,
    }
  );

  return sign;
};

export const parse = (jwtValue: string): IJwt => {
  const result = jwt.verify(jwtValue, env('SESSION_SECRET', 'mySecret'));
  // get the payload
  return result as IJwt;
};

export const expires = (payload: any): number => {
  if (typeof payload.exp === 'undefined') {
    // can't expire if there's no expiry time
    return -1;
  }

  // get the times in seconds
  const now = getUnixTime(new Date());
  const diff = payload.exp - now;
  return diff > 0 ? diff : 0;
};

export const check = (jwt: string): IJwtExtended => {
  if (!jwt) {
    throw new UnauthorizedError(messageNoJwt);
  }

  // get the parsed jwt
  const payload = parse(jwt);
  if (!payload) {
    throw new BadRequestError(messageInvalidJwt);
  }

  // check the expiry date
  const expiresIn = expires(payload);
  if (expiresIn === 0) {
    throw new UnauthorizedError(messageJwtExpired);
  }

  // pass the payload on to the next middleware
  return {
    expiresIn,
    token: jwt,
    ...payload,
  };
};
