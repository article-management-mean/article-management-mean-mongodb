export const baseAPI = '/api/v1';
export const PasswordMin = 5;
export const jwtLife = 0.5 * 3600;
export default {
  article: `/articles`,
  auth: `/auth`,
};
