import express, { Application } from 'express';
import path from 'path';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import methodOverride from 'method-override';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';
import 'express-async-errors';

import db from './api/config/db';
import l from './common/logger';
import errorHandler from './api/middlewares/error.handler';
import route from './routes';

const app = express();

export default class ExpressServer {
  private routes: (app: Application) => void;

  constructor() {
    const root = path.normalize(__dirname + '/..');
    const publicSpec = path.join(root, './public');
    const appSpec = path.join(root, './public', 'article-manager/dist');

    app.use(express.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(
      express.json({
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(
      express.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(methodOverride());
    app.use(cors());
    app.use(helmet());
    app.use(compression());

    app.use(express.static(publicSpec));
    app.use(express.static(appSpec));

    /************************************************************************************
     *                              Serve swagger document content
     ***********************************************************************************/
    const apiSpec = path.join(__dirname, './common', 'api.yml');
    const swaggerDocument = YAML.load(apiSpec);
    app.use(process.env.OPENAPI_SPEC || '/spec', express.static(apiSpec));
    app.use(
      '/api-docs',
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument, {
        explorer: true,
      })
    );

    /************************************************************************************
     *                              Serve API
     ***********************************************************************************/
    route(app);

    /************************************************************************************
     *                              Serve FRONTEND CONTENT
     ***********************************************************************************/
    app.get('*', (_req, res) => {
      res.sendFile(path.join(appSpec, '/index.html'));
    });

    app.use(errorHandler);
  }

  listen(port: number): Application {
    const welcome = (p: number) => (): void =>
      l.info(
        `up and running in ${
          process.env.NODE_ENV || 'development'
        } @: ${os.hostname()} on port: ${p}}`
      );

    db.then((connect) => {
      connect.connection.on('open', () => {
        l.log('MongoDB Connection Successful!');
      });

      connect.connection.on('error', () => {
        l.error('MongoDB connection error:');
      });

      http.createServer(app).listen(port, welcome(port));
    }).catch(() => {
      l.error('MongoDB connect error:');
    });

    return app;
  }
}
