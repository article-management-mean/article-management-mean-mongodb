import { serve } from 'swagger-ui-express';
import './common/env';
import Server from './server';

const port = parseInt(process.env.NODE_PORT ?? '3000');
const server = new Server().listen(port);
export default server;
